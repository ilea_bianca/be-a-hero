package com.hero.ui.map

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import androidx.databinding.DataBindingUtil
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.hero.BR
import com.hero.MapBinding
import com.hero.NoteInfoBinding
import com.hero.R
import com.hero.model.Note
import com.hero.model.NoteCategoryEnum
import com.hero.ui.base.BaseMapFragment
import com.hero.ui.detail.DetailActivity
import com.hero.ui.search.SearchActivity
import org.koin.android.ext.android.inject

class MapFragment : BaseMapFragment<MapViewModel, MapBinding>(R.layout.fragment_map), GoogleMap.OnInfoWindowClickListener {

    private val gson by inject<Gson>()
    override val viewModel by inject<MapViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewModel = viewModel

        setupObservers()
        setupClickListeners()
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        super.onMapReady(googleMap)
        map = googleMap?.apply {
            setInfoWindowAdapter(MapInfoWindowAdapter(requireContext(), gson))
            setOnInfoWindowClickListener(this@MapFragment)
        }
    }

    private fun setupObservers() {
        viewModel.list.observe(viewLifecycleOwner, { list ->
            for (note in list) drawMarker(note)
        })
        viewModel.showError.observe(requireActivity(), { value ->
            if (value) {
                Snackbar.make(binding.root, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG).show()
            }
        })
    }

    private fun setupClickListeners() {
        binding.searchUp.setOnClickListener {
            val term = viewModel.searchTerm.value
            if (!term.isNullOrEmpty()) {
                val intent = Intent(requireContext(), SearchActivity::class.java).apply {
                    putExtra(SearchActivity.SEARCH_TERM, term)
                }
                startActivity(intent)
            }
        }

        binding.input.setOnKeyListener { _, keyCode, event ->
            if ((event.action == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                binding.searchUp.performClick()
            }
            false
        }
    }

    override fun onCameraIdle() {
        try {
            map?.projection?.visibleRegion?.latLngBounds?.let { latLng ->
                viewModel.getNotesByLatLng(latLng)
            }
        } catch (e: Exception) {
            Snackbar.make(binding.root, requireContext().getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG).show()
        }
    }

    private fun drawMarker(note: Note) {
        val location = LatLng(note.latitude, note.longitude)
        map?.addMarker(
            MarkerOptions()
                .position(location)
                .icon(BitmapDescriptorFactory.defaultMarker(if (note.offer) BitmapDescriptorFactory.HUE_AZURE else BitmapDescriptorFactory.HUE_VIOLET))
                .snippet(gson.toJson(note))
        )
    }

    override fun onInfoWindowClick(marker: Marker) {
        val intent = Intent(requireContext(), DetailActivity::class.java).apply {
            putExtra(DetailActivity.NOTE_DETAIL_JSON_EXTRA, marker.snippet)
        }
        startActivity(intent)
    }

    class MapInfoWindowAdapter(private val context: Context, private val gson: Gson) : GoogleMap.InfoWindowAdapter {
        override fun getInfoWindow(marker: Marker): View? {
            return null
        }

        override fun getInfoContents(marker: Marker): View {
            val noteInfoBinding: NoteInfoBinding =
                DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.map_info_window, null, true)
            val note = gson.fromJson(marker.snippet, Note::class.java)
            noteInfoBinding.setVariable(BR.note, note)
            noteInfoBinding.setVariable(BR.categories, mapCategoriesToString(note.category))
            noteInfoBinding.executePendingBindings()
            return noteInfoBinding.root
        }

        @SuppressLint("ResourceType")
        private fun mapCategoriesToString(categories: List<Long>): String {
            var categoriesString = ""
            for (iterator in 0 until categories.size - 1) {
                categoriesString += context.getString(NoteCategoryEnum.getEnum(categories[iterator]).stringRes) + ", "
            }
            categoriesString += context.getString(NoteCategoryEnum.getEnum(categories.last()).stringRes)
            return categoriesString
        }
    }

    companion object {
        const val FRAGMENT_TAG = "MapFragment"

        fun newInstance() = MapFragment()
    }
}