package com.hero.ui.map

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.LatLngBounds
import com.hero.model.Note
import com.hero.repository.ApplicationRepository
import com.hero.repository.Result
import com.hero.ui.base.BaseViewModel
import com.hero.util.NetworkMonitor
import kotlinx.coroutines.launch

class MapViewModel(networkMonitor: NetworkMonitor, private val repository: ApplicationRepository) :
    BaseViewModel(networkMonitor) {

    private val _list = MutableLiveData<List<Note>>()
    val list: LiveData<List<Note>> = _list

    private val shownList = mutableListOf<Note>()

    private val _showError = MutableLiveData<Boolean>()
    val showError: LiveData<Boolean> = _showError

    val shouldShowCancel: LiveData<Boolean> get() = _shouldShowCancel
    private val _shouldShowCancel = MutableLiveData<Boolean>()
    val searchTerm = MutableLiveData<String>()

    private val searchObserver = Observer<String> { term ->
        _shouldShowCancel.value = !term.isNullOrEmpty()
    }

    init {
        viewModelScope.launch {
            repository.getNotesByUser()
        }
        searchTerm.observeForever(searchObserver)
    }

    @Suppress("UNCHECKED_CAST")
    fun getNotesByLatLng(value: LatLngBounds) {
        val minLat = value.southwest.latitude
        val minLng = value.southwest.longitude
        val maxLat = value.northeast.latitude
        val maxLng = value.northeast.longitude
        viewModelScope.launch {
            repository.getNotesByLatLng(minLat, maxLat, minLng, maxLng).also { result ->
                if (result is Result.Success) {
                    try {
                        val list = computeListDifference(result.content as List<Note>, shownList)
                        _list.value = list
                        shownList.addAll(list)
                    } catch (e: Exception) {
                        _showError.value = true
                    }
                } else {
                    _showError.value = true
                }
            }
        }
    }

    private fun computeListDifference(firstList: List<Note>, secondList: List<Note>): List<Note> =
        firstList.toSet().minus(secondList.toSet()).toList()

    fun onDeleteClicked() {
        searchTerm.value = ""
    }

    override fun onCleared() {
        super.onCleared()
        searchTerm.removeObserver(searchObserver)
    }
}