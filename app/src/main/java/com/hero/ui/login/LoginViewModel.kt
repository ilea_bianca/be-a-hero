package com.hero.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseUser
import com.hero.repository.LoginRepository
import com.hero.repository.Result
import com.hero.ui.base.BaseViewModel
import com.hero.util.NetworkMonitor
import com.hero.util.SingleLiveEvent
import kotlinx.coroutines.launch

class LoginViewModel(networkMonitor: NetworkMonitor, private val loginRepository: LoginRepository) :
    BaseViewModel(networkMonitor) {

    private val _userLoggedIn = SingleLiveEvent<Boolean>()
    val userLoggedIn: LiveData<Boolean> = _userLoggedIn

    private val _showProgress = MutableLiveData<Boolean>()
    val showProgress: LiveData<Boolean> = _showProgress

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error
    private val _showError = MutableLiveData<Boolean>()
    val showError: LiveData<Boolean> = _showError

    fun handleUser(user: FirebaseUser?) {
        var email = user?.email
        if (email.isNullOrEmpty() && user != null) {
            for (profile in user.providerData) {
                profile.email?.also { email = it }
            }
        }
        email.let {
            if (it != null && it.isNotEmpty()) {
                saveUser(it)
            } else _showProgress.value = false
        }

    }

    private fun saveUser(email: String) {
        viewModelScope.launch {
            loginRepository.saveUser(email).also { result ->
                _showProgress.value = false
                if (result is Result.Success) {
                    _userLoggedIn.value = true
                }
            }
        }
    }

    fun setShowProgress(value: Boolean) {
        _showProgress.value = value
    }

    fun setError(error: String) {
        _error.value = error
        _showProgress.value = false
        setShowError(true)
    }

    fun setShowError(value: Boolean) {
        _showError.value = value
    }

    fun preventClicks() {
        return
    }

    fun isFirstLogin() = loginRepository.isFirstLogin()
}