package com.hero.ui.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.hero.LoginBinding
import com.hero.R
import com.hero.ui.MainActivity
import com.hero.ui.base.BaseActivity
import com.hero.ui.info.InfoActivity
import org.koin.android.ext.android.inject

class LoginActivity : BaseActivity<LoginViewModel, LoginBinding>(R.layout.activity_login) {
    override val viewModel: LoginViewModel by inject()

    private lateinit var googleSignInOptions: GoogleSignInOptions
    private lateinit var googleSignInClient: GoogleSignInClient
    private lateinit var facebookCallbackManager: CallbackManager
    private val firebaseAuth: FirebaseAuth by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.viewModel = viewModel

        setupGoogleSignIn()
        setupFacebookSignIn()

        viewModel.userLoggedIn.observe(this, {
            if (viewModel.isFirstLogin()) {
                startActivity(Intent(this, InfoActivity::class.java))
            } else {
                startActivity(Intent(this, MainActivity::class.java))
            }
        })
    }

    override fun onStart() {
        super.onStart()
        val currentUser = firebaseAuth.currentUser
        viewModel.handleUser(currentUser)
    }

    override fun onResume() {
        super.onResume()
        viewModel.setShowProgress(false)
    }

    private fun setupGoogleSignIn() {
        googleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.web_client_id))
            .requestEmail()
            .build()
        googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions)
        binding.googleSignInButton.setOnClickListener {
            viewModel.setShowError(false)
            viewModel.setShowProgress(true)
            startActivityForResult(googleSignInClient.signInIntent, RC_SIGN_IN)
        }
    }

    private fun setupFacebookSignIn() {
        facebookCallbackManager = CallbackManager.Factory.create()
        binding.facebookSignInButton.setReadPermissions("email", "public_profile")
        binding.facebookSignInButtonCustom.setOnClickListener {
            viewModel.setShowError(false)
            viewModel.setShowProgress(true)
            binding.facebookSignInButton.performClick()
        }
        handleFacebookSignInResult()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        facebookCallbackManager.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            handleGoogleSignInResult(data)
        }
    }

    private fun handleGoogleSignInResult(data: Intent?) {
        try {
            GoogleSignIn.getSignedInAccountFromIntent(data).getResult(ApiException::class.java)
                ?.let { account ->
                    handleGoogleSignInSuccess(account)
                }
        } catch (e: ApiException) {
            viewModel.setError(this.getString(R.string.sign_in_google_failed))
        }
    }

    private fun handleFacebookSignInResult() {
        binding.facebookSignInButton.registerCallback(
            facebookCallbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(result: LoginResult) {
                    handleFacebookSignInSuccess(result.accessToken)
                }

                override fun onCancel() {}
                override fun onError(error: FacebookException?) {
                    Log.d("Login", "facebook error $error")
                    viewModel.setError(this@LoginActivity.getString(R.string.sign_in_facebook_failed))
                }
            })
    }

    private fun handleGoogleSignInSuccess(account: GoogleSignInAccount) {
        val credential = GoogleAuthProvider.getCredential(account.idToken, null)
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener(this) { task ->
            if (task.isSuccessful) {
                viewModel.handleUser(firebaseAuth.currentUser)
            } else {
                viewModel.setError(this.getString(R.string.sign_in_google_failed))
            }
        }
    }

    private fun handleFacebookSignInSuccess(token: AccessToken) {
        val credential = FacebookAuthProvider.getCredential(token.token)
        Log.d("Login", "handleFacebookSignInSuccess $credential")
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener(this) { task ->
            if (task.isSuccessful) {
                viewModel.handleUser(firebaseAuth.currentUser)
            } else {
                Log.d("Login", "facebook error task not success ${task.exception}")
                viewModel.setError(this.getString(R.string.sign_in_facebook_failed))
            }
        }
    }

    companion object {
        private const val RC_SIGN_IN = 10
    }
}