package com.hero.ui.addNote

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import com.hero.AddNoteBinding
import com.hero.R
import com.hero.ui.base.BaseActivity
import org.koin.android.ext.android.inject

class AddNoteActivity : BaseActivity<AddNoteViewModel, AddNoteBinding>(R.layout.activity_add_note) {
    override val viewModel: AddNoteViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.viewModel = viewModel

        getLocationExtra()
        setupObservers()
        setupClickListener()
    }

    private fun getLocationExtra() {
        val latitude = intent.extras?.getDouble(NOTE_LOCATION_LAT_EXTRA) ?: 0.0
        val longitude = intent.extras?.getDouble(NOTE_LOCATION_LNG_EXTRA) ?: 0.0
        val name = intent.extras?.getString(NOTE_LOCATION_NAME_EXTRA) ?: ""
        viewModel.setLocationData(latitude, longitude, name)
    }

    private fun setupObservers() {
        viewModel.addNoteSuccess.observe(this, {
            finish()
        })

        viewModel.addNoteError.observe(this, {
            Snackbar.make(binding.root, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG).show()
        })
    }

    private fun setupClickListener() {
        binding.addNoteButton.setOnClickListener {
            viewModel.saveNote()
        }
    }

    companion object {
        const val NOTE_LOCATION_LAT_EXTRA = "noteLocationLatExtra"
        const val NOTE_LOCATION_LNG_EXTRA = "noteLocationLngExtra"
        const val NOTE_LOCATION_NAME_EXTRA = "noteLocationNameExtra"
    }
}