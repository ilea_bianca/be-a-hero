package com.hero.ui.addNote

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hero.R
import com.hero.model.NoteCategoryEnum
import com.hero.repository.ApplicationRepository
import com.hero.repository.Result
import com.hero.ui.base.BaseViewModel
import com.hero.util.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AddNoteViewModel(networkMonitor: NetworkMonitor, private val applicationRepository: ApplicationRepository) : BaseViewModel(networkMonitor) {

    var note = ValidatorInputField("", Validator.NotEmptyValidator, R.string.add_note_description_error)
    var name = ValidatorInputField("", Validator.NotEmptyValidator, R.string.add_note_name_error)
    var phone = ValidatorInputField("", Validator.Phone, R.string.add_note_phone_error)

    var offerValue = MutableLiveData<Boolean>().apply { value = false }
    val isFoodCategory = MutableLiveData<Boolean>()
    val isMedicalCategory = MutableLiveData<Boolean>()
    val isOtherCategory = MutableLiveData<Boolean>()
    var categoryError = mediatorLiveData(isFoodCategory, isMedicalCategory, isOtherCategory) {
        (isFoodCategory.value == null || isFoodCategory.value == false) &&
                (isMedicalCategory.value == null || isMedicalCategory.value == false) &&
                (isOtherCategory.value == null || isOtherCategory.value == false)
    }

    private var latitude: Double = 0.0
    private var longitude: Double = 0.0
    val location = ObservableField<String>()

    val isLoading: LiveData<Boolean> get() = _isLoading
    private var _isLoading = MutableLiveData<Boolean>()

    val addNoteSuccess: LiveData<Boolean> get() = _addNoteSuccess
    private val _addNoteSuccess = SingleLiveEvent<Boolean>()

    val addNoteError: LiveData<Boolean> get() = _addNoteError
    private val _addNoteError = SingleLiveEvent<Boolean>()

    fun setLocationData(latitude: Double, longitude: Double, location: String) {
        this.latitude = latitude
        this.longitude = longitude
        this.location.set(location)
    }

    fun saveNote() {
        verifyNetworkAvailability()
        if (isNetworkAvailable.value != true) return

        note.validate()
        phone.validate()
        name.validate()
        if (categoryError.value == true) return

        if (note.isValid && phone.isValid && name.isValid) {
            _isLoading.value = true

            val location = location.get() ?: EMPTY_STRING
            val offer = offerValue.value ?: true
            val name = name.value ?: EMPTY_STRING
            val phone = phone.value ?: EMPTY_STRING
            val text = note.value ?: EMPTY_STRING

            viewModelScope.launch {
                applicationRepository.saveNote(latitude, longitude, location, offer, getCategories(), name, phone, text).also { result ->
                    withContext(Dispatchers.Main) {
                        _isLoading.value = false
                        when (result) {
                            is Result.Success<*> -> _addNoteSuccess.value = true
                            else -> _addNoteError.value = true
                        }
                    }
                }
            }
        }
    }

    private fun verifyNetworkAvailability() {
        if (isNetworkAvailable.value != true) {
            _addNoteError.value = true
        }
    }

    private fun getCategories(): List<Long> {
        val list = mutableListOf<Long>()
        if (isFoodCategory.value == true) list.add(NoteCategoryEnum.FOOD.value)
        if (isMedicalCategory.value == true) list.add(NoteCategoryEnum.MEDICAL_EQUIPMENT.value)
        if (isOtherCategory.value == true) list.add(NoteCategoryEnum.OTHER.value)
        return list
    }

    fun preventClicks() {
        return
    }

    companion object {
        private const val EMPTY_STRING = ""
    }
}