package com.hero.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hero.model.Note
import com.hero.model.NoteCategoryEnum
import com.hero.repository.ApplicationRepository
import com.hero.repository.Result
import com.hero.ui.base.BaseViewModel
import com.hero.util.NetworkMonitor
import kotlinx.coroutines.launch

class DetailViewModel(networkMonitor: NetworkMonitor, private val repository: ApplicationRepository) :
    BaseViewModel(networkMonitor) {

    val note = MutableLiveData<Note>()
    val isFoodCategory = MutableLiveData<Boolean>()
    val isMedicalCategory = MutableLiveData<Boolean>()
    val isOtherCategory = MutableLiveData<Boolean>()

    private val _showError = MutableLiveData<Boolean>()
    val showError: LiveData<Boolean> = _showError

    fun setNote(note: Note) {
        this.note.value = note
        isFoodCategory.value = note.category.contains(NoteCategoryEnum.FOOD.value)
        isMedicalCategory.value = note.category.contains(NoteCategoryEnum.MEDICAL_EQUIPMENT.value)
        isOtherCategory.value = note.category.contains(NoteCategoryEnum.OTHER.value)
    }

    fun setNoteId(noteId: String) {
        viewModelScope.launch {
            repository.getNotesById(noteId).also { result ->
                if (result is Result.Success) {
                    try {
                        (result.content as Note).let { item ->
                            note.value = item
                            isFoodCategory.value = item.category.contains(NoteCategoryEnum.FOOD.value)
                            isMedicalCategory.value = item.category.contains(NoteCategoryEnum.MEDICAL_EQUIPMENT.value)
                            isOtherCategory.value = item.category.contains(NoteCategoryEnum.OTHER.value)
                        }
                    } catch (e: Exception) {
                        _showError.value = true
                    }
                } else {
                    _showError.value = true
                }
            }
        }
    }
}