package com.hero.ui.detail

import android.os.Bundle
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.hero.DetailBinding
import com.hero.R
import com.hero.model.Note
import com.hero.ui.base.BaseActivity
import org.koin.android.ext.android.inject

class DetailActivity : BaseActivity<DetailViewModel, DetailBinding>(R.layout.activity_detail) {
    private val gson by inject<Gson>()
    override val viewModel: DetailViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.viewModel = viewModel

        val noteJson = intent.extras?.getString(NOTE_DETAIL_JSON_EXTRA)
        noteJson?.let { viewModel.setNote(gson.fromJson(noteJson, Note::class.java)) }

        val noteId = intent.extras?.getString(NOTE_DETAIL_ID_EXTRA)
        noteId?.let { viewModel.setNoteId(noteId) }

        viewModel.showError.observe(this, { value ->
            if (value) {
                Snackbar.make(binding.root, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG).show()
            }
        })
    }

    companion object {
        const val NOTE_DETAIL_JSON_EXTRA = "noteDetailJsonExtra"
        const val NOTE_DETAIL_ID_EXTRA = "noteDetailIdExtra"
    }
}