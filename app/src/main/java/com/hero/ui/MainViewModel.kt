package com.hero.ui

import com.hero.ui.base.BaseViewModel
import com.hero.util.NetworkMonitor

class MainViewModel(networkMonitor: NetworkMonitor) : BaseViewModel(networkMonitor)