package com.hero.ui.info

import android.content.Intent
import android.os.Bundle
import com.hero.InfoBinding
import com.hero.R
import com.hero.ui.MainActivity
import com.hero.ui.base.BaseActivity
import org.koin.android.ext.android.inject

class InfoActivity : BaseActivity<InfoViewModel, InfoBinding>(R.layout.activity_info) {
    override val viewModel: InfoViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.viewModel = viewModel

        binding.continueButton.setOnClickListener {
            startActivityIntent(Intent(this, MainActivity::class.java))
        }
    }
}