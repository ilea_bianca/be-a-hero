package com.hero.ui.info

import com.hero.repository.ApplicationRepository
import com.hero.ui.base.BaseViewModel
import com.hero.util.NetworkMonitor

class InfoViewModel(networkMonitor: NetworkMonitor, repository: ApplicationRepository) : BaseViewModel(networkMonitor) {

    init {
        repository.setFirstLogin(false)
    }
}