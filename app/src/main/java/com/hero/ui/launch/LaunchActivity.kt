package com.hero.ui.launch

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.hero.R
import com.hero.ui.MainActivity
import com.hero.ui.info.InfoActivity
import com.hero.ui.login.LoginActivity
import org.koin.android.ext.android.inject

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class LaunchActivity : AppCompatActivity() {

    private val viewModel: LaunchViewModel by inject()
    private val firebaseAuth: FirebaseAuth by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)

        viewModel.userLoggedIn.observe(this, { value ->
            when {
                value && viewModel.isFirstLogin() -> startActivityIntent(
                    Intent(
                        this,
                        InfoActivity::class.java
                    )
                )
                value && !viewModel.isFirstLogin() -> startActivityIntent(
                    Intent(
                        this,
                        MainActivity::class.java
                    )
                )
                else -> startActivityIntent(Intent(this, LoginActivity::class.java))
            }
        })
        viewModel.handleUser(firebaseAuth.currentUser)
    }

    private fun startActivityIntent(intent: Intent) = startActivity(intent.apply {
        flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
    }).also { finishAfterTransition() }
}