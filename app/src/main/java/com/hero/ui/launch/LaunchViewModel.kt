package com.hero.ui.launch

import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseUser
import com.hero.repository.LoginRepository
import com.hero.repository.Result
import com.hero.ui.base.BaseViewModel
import com.hero.util.NetworkMonitor
import com.hero.util.SingleLiveEvent
import kotlinx.coroutines.launch

class LaunchViewModel(networkMonitor: NetworkMonitor, private val loginRepository: LoginRepository) :
    BaseViewModel(networkMonitor) {

    private val _userLoggedIn = SingleLiveEvent<Boolean>()
    val userLoggedIn: LiveData<Boolean> = _userLoggedIn

    fun handleUser(user: FirebaseUser?) {
        var email = user?.email
        if (email.isNullOrEmpty() && user != null) {
            for (profile in user.providerData) {
                profile.email?.also { email = it }
            }
        }
        email.let {
            if (it != null && it.isNotEmpty()) {
                saveUser(it)
            } else {
                _userLoggedIn.value = false
            }
        }
    }

    private fun saveUser(email: String) {
        viewModelScope.launch {
            loginRepository.saveUser(email).also { result ->
                _userLoggedIn.value = result is Result.Success
            }
        }
    }

    fun isFirstLogin() = loginRepository.isFirstLogin()
}