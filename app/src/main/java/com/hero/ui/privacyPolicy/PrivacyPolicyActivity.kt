package com.hero.ui.privacyPolicy

import android.annotation.SuppressLint
import android.os.Bundle
import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity
import com.hero.R

class PrivacyPolicyActivity : AppCompatActivity() {

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_privacy_policy)
        findViewById<WebView>(R.id.web_view).apply {
            settings.javaScriptEnabled = true
            settings.allowFileAccess = true
            settings.useWideViewPort = true
            settings.loadWithOverviewMode = true
            settings.builtInZoomControls = true
            loadUrl(PRIVACY_POLICY_URL)
        }
    }

    companion object {
        private const val PRIVACY_POLICY_URL = "https://be-a-hero.flycricket.io/privacy.html"
    }
}