package com.hero.ui.base

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.view.View
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat
import androidx.databinding.ViewDataBinding
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.hero.R

abstract class BaseMapFragment<VM : BaseViewModel, VB : ViewDataBinding>(@LayoutRes layoutRes: Int) : BaseFragment<VM, VB>(layoutRes), OnMapReadyCallback,
    GoogleMap.OnCameraIdleListener {

    private lateinit var settingsClient: SettingsClient
    private lateinit var locationManager: LocationManager
    private val locationRequest: LocationRequest = LocationRequest.create()
    private val locationSettingsRequest: LocationSettingsRequest

    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var mapFragment: SupportMapFragment
    protected var map: GoogleMap? = null

    init {
        locationRequest.apply {
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            interval = 10 * 1000.toLong()
            fastestInterval = 2 * 1000.toLong()
        }
        locationSettingsRequest = LocationSettingsRequest.Builder().addLocationRequest(locationRequest).setAlwaysShow(true).build()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        settingsClient = LocationServices.getSettingsClient(requireContext())
        locationManager = requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        setupMapFragment()
    }

    private fun setupMapFragment() {
        mapFragment = childFragmentManager.findFragmentById(R.id.map_view) as SupportMapFragment
        mapFragment.getMapAsync(this)
        setupLocationButton()
    }

    private fun setupLocationButton() {
        mapFragment.view?.findViewWithTag<View>("GoogleMapMyLocationButton")?.apply {
            (layoutParams as RelativeLayout.LayoutParams).apply {
                addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
                addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
                addRule(RelativeLayout.ALIGN_PARENT_END, RelativeLayout.TRUE)
                addRule(RelativeLayout.ALIGN_PARENT_START, 0)
            }
            setPadding(0, 0, 0, 200)
        }
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        map = googleMap?.apply {
            mapType = GoogleMap.MAP_TYPE_NORMAL
            setOnCameraIdleListener(this@BaseMapFragment)
        }
        requestLocationPermissionAndProviderAndEnableLocation()
    }

    private fun requestLocationPermissionAndProviderAndEnableLocation() = when {
        shouldRequestLocationPermission() -> requestLocationPermission()
        shouldRequestLocationProvider() -> requestLocationProvider()
        else -> enableLocation()
    }

    private fun shouldRequestLocationPermission() =
        ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED

    private fun requestLocationPermission() = requestPermissions(
        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION),
        REQUEST_LOCATION_PERMISSIONS_CODE
    )

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode != REQUEST_LOCATION_PERMISSIONS_CODE) return

        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            requestLocationProviderAndEnableLocation()
        }
    }

    private fun requestLocationProviderAndEnableLocation() {
        if (shouldRequestLocationProvider()) {
            requestLocationProvider()
        } else {
            enableLocation()
        }
    }

    private fun shouldRequestLocationProvider() = !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)

    private fun requestLocationProvider() {
        settingsClient.checkLocationSettings(locationSettingsRequest).addOnSuccessListener(requireActivity()) {
            enableLocation()
        }.addOnFailureListener(requireActivity()) { error ->
            requestLocationProviderFailure(error)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_LOCATION_PROVIDER_CODE) {
            enableLocation()
        }
    }

    @SuppressLint("MissingPermission")
    private fun enableLocation() {
        map?.isMyLocationEnabled = true
        getLastLocation()
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        fusedLocationProviderClient.lastLocation.addOnSuccessListener { location ->
            location?.let {
                val latLng = LatLng(location.latitude, location.longitude)
                map?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, MAP_CAMERA_ZOOM))
            }
        }
    }

    private fun requestLocationProviderFailure(e: Exception) {
        when ((e as ApiException).statusCode) {
            LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                try {
                    val resolvable = e as ResolvableApiException
                    startIntentSenderForResult(resolvable.resolution.intentSender, REQUEST_LOCATION_PROVIDER_CODE, null, 0, 0, 0, null)
                } catch (sie: IntentSender.SendIntentException) {
                }
            }
            LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                val errorMessage = "Location settings are inadequate, and cannot be fixed here. Fix in Settings."
                Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show()
            }
        }
    }

    private companion object {
        const val REQUEST_LOCATION_PERMISSIONS_CODE = 60
        const val REQUEST_LOCATION_PROVIDER_CODE = 102
        const val MAP_CAMERA_ZOOM = 15f
    }
}