package com.hero.ui.base

import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecyclerAdapter<T, VH : BaseRecyclerAdapter.BaseViewHolder<T>> : RecyclerView.Adapter<VH>() {

    private var items = mutableListOf<T>()

    fun setItems(newItems: List<T>?) {
        requireNotNull(newItems) { "Cannot set `null` item to the Recycler adapter" }
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    fun addItem(item: T) {
        items.add(item)
        notifyItemInserted(items.size - 1)
    }

    fun getItems() = items

    fun getItem(position: Int): T = items[position]

    override fun getItemCount() = items.size

    @LayoutRes
    abstract fun getLayoutResId(): Int

    abstract override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.onBind(items[position])
    }

    abstract class BaseViewHolder<T>(binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        abstract fun onBind(item: T)
    }
}
