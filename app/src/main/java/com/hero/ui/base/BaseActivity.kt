package com.hero.ui.base

import android.content.Intent
import android.os.Bundle
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.hero.util.NetworkMonitor
import org.koin.android.ext.android.inject

abstract class BaseActivity<VM : BaseViewModel, VB : ViewDataBinding>(@LayoutRes private val layoutRes: Int) : AppCompatActivity() {

    private val networkMonitor: NetworkMonitor by inject()
    protected lateinit var binding: VB
    protected abstract val viewModel: VM?

    override fun onCreate(savedInstanceState: Bundle?) {
        window.enterTransition?.apply {
            excludeTarget(android.R.id.statusBarBackground, true)
            excludeTarget(android.R.id.navigationBarBackground, true)
        }
        window.returnTransition?.apply {
            excludeTarget(android.R.id.statusBarBackground, true)
            excludeTarget(android.R.id.navigationBarBackground, true)
        }

        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, layoutRes)
        binding.lifecycleOwner = this
    }

    override fun onStart() {
        super.onStart()
        networkMonitor.registerNetworkCallback()
    }

    override fun onStop() {
        super.onStop()
        networkMonitor.unregisterNetworkCallback()
    }

    protected open fun addFragment(@IdRes containerViewId: Int, fragment: Fragment, fragmentTag: String) {
        supportFragmentManager
            .beginTransaction()
            .add(containerViewId, fragment, fragmentTag)
            .disallowAddToBackStack()
            .commit()
    }

    protected open fun replaceFragment(@IdRes containerViewId: Int, fragment: Fragment, fragmentTag: String, backStackStateName: String? = null) {
        supportFragmentManager
            .beginTransaction()
            .replace(containerViewId, fragment, fragmentTag)
            .addToBackStack(backStackStateName)
            .commit()
    }

    protected fun startActivityIntent(intent: Intent) = startActivity(intent.apply {
        flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
    }).also { closeActivity() }

    private fun closeActivity() = finishAfterTransition()
}