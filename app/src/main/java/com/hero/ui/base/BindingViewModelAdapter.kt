package com.hero.ui.base

import android.content.Context
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.OnRebindCallback
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.hero.BR
import com.hero.util.layoutInflater

abstract class BindingViewModelAdapter<VB : ViewDataBinding, VM : Any> : RecyclerView.Adapter<BindingViewModelAdapter.BindingViewHolder<VB, VM>>() {

    protected var recyclerView: RecyclerView? = null
    private var rebindCallback: OnRebindCallback<VB>?
    private var adapterItemClickListener: ItemClickListener = {}

    init {
        rebindCallback = object : OnRebindCallback<VB>() {
            override fun onPreBind(binding: VB): Boolean = recyclerView?.let { recyclerView ->
                val childAdapterPosition = recyclerView.getChildAdapterPosition(binding.root)
                val shouldRebind = recyclerView.isComputingLayout || childAdapterPosition == RecyclerView.NO_POSITION
                if (!shouldRebind) {
                    notifyItemChanged(childAdapterPosition, DB_PAYLOAD)
                }
                shouldRebind
            } ?: true
        }
    }

    fun setOnAdapterItemClickListener(itemClickListener: ItemClickListener) {
        this.adapterItemClickListener = itemClickListener
    }

    @LayoutRes
    protected abstract fun getItemLayoutId(position: Int): Int

    protected abstract fun bindItem(holder: BindingViewHolder<VB, VM>, position: Int, payloads: List<Any>)

    protected abstract fun createViewModel(context: Context, @LayoutRes viewType: Int): VM?

    @CallSuper
    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        this.recyclerView = recyclerView
    }

    @CallSuper
    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        this.rebindCallback = null
        this.recyclerView = null
    }

    override fun onBindViewHolder(holder: BindingViewHolder<VB, VM>, position: Int, payloads: MutableList<Any>) {
        // when a VH is rebound to the same item, we don't have to call the setters
        if (payloads.isEmpty() || payloads.any { it !== DB_PAYLOAD }) {
            bindItem(holder, position, payloads)
        }
        holder.binding.executePendingBindings()
    }

    final override fun onBindViewHolder(holder: BindingViewHolder<VB, VM>, position: Int) {
        throw IllegalArgumentException("Just overridden to make final.")
    }

    @CallSuper
    override fun onCreateViewHolder(parent: ViewGroup, @LayoutRes viewType: Int): BindingViewHolder<VB, VM> {
        val viewHolder = BindingViewHolder.create<VB, VM>(parent, viewType, createViewModel(parent.context, viewType))
        rebindCallback?.let { viewHolder.binding.addOnRebindCallback(it) }
        viewHolder.setItemClickListener(adapterItemClickListener)
        return viewHolder
    }

    final override fun getItemViewType(position: Int) = getItemLayoutId(position)

    companion object {
        private val DB_PAYLOAD = Any()
    }

    class BindingViewHolder<out VB : ViewDataBinding, out VM : Any>
    private constructor(val binding: VB, val viewModel: VM?) : RecyclerView.ViewHolder(binding.root) {

        init {
            viewModel?.let {
                binding.setVariable(BR.viewModel, viewModel)
            }
        }

        fun setItemClickListener(itemClickListener: ItemClickListener) {
            binding.root.setOnClickListener {
                if (bindingAdapterPosition != RecyclerView.NO_POSITION) {
                    itemClickListener.invoke(bindingAdapterPosition)
                }
            }
        }

        companion object {
            fun <VB : ViewDataBinding, VM : Any> create(parent: ViewGroup, @LayoutRes layoutId: Int, viewModel: VM?): BindingViewHolder<VB, VM> {
                val binding: VB = DataBindingUtil.inflate(parent.context.layoutInflater(), layoutId, parent, false)
                return BindingViewHolder(binding, viewModel)
            }
        }
    }
}

typealias ItemClickListener = (position: Int) -> Unit