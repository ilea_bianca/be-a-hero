package com.hero.ui.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.hero.util.NetworkMonitor

open class BaseViewModel(networkMonitor: NetworkMonitor) : ViewModel() {

    val isNetworkAvailable: LiveData<Boolean> = Transformations.map(networkMonitor.isNetworkConnected) { it }
}