package com.hero.ui.pin

import android.location.Address
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng
import com.hero.ui.base.BaseViewModel
import com.hero.util.NetworkMonitor

class PinViewModel(networkMonitor: NetworkMonitor) : BaseViewModel(networkMonitor) {

    var location: LatLng? = null

    private var _locationName = MutableLiveData<String>()
    val locationName: LiveData<String> = _locationName

    private val _showError = MutableLiveData<Boolean>()
    val showError: LiveData<Boolean> = _showError

    fun setDefaultLocationName(location: String) {
        _locationName.value = location
    }

    fun setCurrentLocation(newLocation: LatLng) {
        location = newLocation
    }

    fun setLocationName(addresses: List<Address>) {
        if (addresses.isNotEmpty()) {
            _locationName.value = addresses[0].getAddressLine(0)
        }
        _showError.value = true
    }

    fun showError(value: Boolean) {
        _showError.value = value
    }
}