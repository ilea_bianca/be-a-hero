package com.hero.ui.pin

import android.content.Intent
import android.location.Geocoder
import android.os.Bundle
import android.view.View
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.material.snackbar.Snackbar
import com.hero.PinBinding
import com.hero.R
import com.hero.ui.addNote.AddNoteActivity
import com.hero.ui.base.BaseMapFragment
import org.koin.android.ext.android.inject
import java.util.*

class PinFragment : BaseMapFragment<PinViewModel, PinBinding>(R.layout.fragment_pin) {
    override val viewModel by inject<PinViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewModel = viewModel
        viewModel.setDefaultLocationName(requireContext().getString(R.string.add_note_location_not_selected))

        binding.addNoteButton.setOnClickListener {
            val latitude = viewModel.location?.latitude
            val longitude = viewModel.location?.longitude
            if (latitude == null || longitude == null || viewModel.locationName.value.isNullOrEmpty()) {
                viewModel.showError(true)
            } else {
                val intent = Intent(requireContext(), AddNoteActivity::class.java).apply {
                    putExtra(AddNoteActivity.NOTE_LOCATION_LAT_EXTRA, latitude)
                    putExtra(AddNoteActivity.NOTE_LOCATION_LNG_EXTRA, longitude)
                    putExtra(AddNoteActivity.NOTE_LOCATION_NAME_EXTRA, viewModel.locationName.value)
                }
                startActivity(intent)
            }
        }
    }

    override fun onCameraIdle() {
        viewModel.showError(false)

        val currentLocation = map?.cameraPosition?.target
        if (currentLocation == null) {
            viewModel.showError(true)
        } else {
            try {
                viewModel.setCurrentLocation(currentLocation)
                viewModel.setLocationName(
                    Geocoder(
                        context,
                        Locale.getDefault()
                    ).getFromLocation(currentLocation.latitude, currentLocation.longitude, 1)
                )
                map?.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 15f))
            } catch (e: Exception) {
                Snackbar.make(
                    binding.root,
                    requireContext().getString(R.string.something_went_wrong),
                    Snackbar.LENGTH_LONG
                ).show()
            }
        }
    }

    companion object {
        const val FRAGMENT_TAG = "PinFragment"

        fun newInstance() = PinFragment()
    }
}