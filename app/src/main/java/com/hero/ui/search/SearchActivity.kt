package com.hero.ui.search

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.hero.R
import com.hero.SearchBinding
import com.hero.ui.base.BaseActivity
import com.hero.ui.detail.DetailActivity
import org.koin.android.ext.android.inject

class SearchActivity : BaseActivity<SearchViewModel, SearchBinding>(R.layout.activity_search) {
    override val viewModel: SearchViewModel by inject()
    private val searchAdapter = SearchRecyclerAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.viewModel = viewModel

        val searchTerm = intent.extras?.getString(SEARCH_TERM) ?: ""
        viewModel.setSearchTerm(searchTerm)

        binding.searchRecycler.layoutManager = LinearLayoutManager(this)
        binding.searchRecycler.setHasFixedSize(true)
        binding.searchRecycler.adapter = searchAdapter.apply {
            clickListener = { noteId ->
                val intent = Intent(this@SearchActivity, DetailActivity::class.java).apply {
                    putExtra(DetailActivity.NOTE_DETAIL_ID_EXTRA, noteId)
                }
                startActivity(intent)
            }
        }

        viewModel.list.observe(this, { list ->
            searchAdapter.addItems(list)
        })
    }

    companion object {
        const val SEARCH_TERM = "searchTerm"
    }
}