package com.hero.ui.search

import android.content.Context
import android.view.ViewGroup
import com.hero.R
import com.hero.databinding.SearchNoteItemBinding
import com.hero.model.Note
import com.hero.ui.base.BindingViewModelAdapter

class SearchRecyclerAdapter : BindingViewModelAdapter<SearchNoteItemBinding, SearchNoteViewModel>() {

    private val items = mutableListOf<Note>()
    var clickListener: (String) -> Unit = { }

    fun addItems(itemList: List<Note>) {
        items.clear()
        items.addAll(itemList)
        notifyDataSetChanged()
    }

    override fun getItemLayoutId(position: Int) = R.layout.item_search_note

    override fun bindItem(holder: BindingViewHolder<SearchNoteItemBinding, SearchNoteViewModel>, position: Int, payloads: List<Any>) {
        (holder.viewModel as SearchNoteViewModel).setItem(items[position])
    }

    override fun createViewModel(context: Context, viewType: Int) = SearchNoteViewModel()

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingViewHolder<SearchNoteItemBinding, SearchNoteViewModel> {
        return super.onCreateViewHolder(parent, viewType).apply {
            val noteViewModel = viewModel ?: return@apply
            binding.searchItem.setOnClickListener {
                noteViewModel.note.get()?.let { note ->
                    clickListener(note.id)
                }
            }
        }
    }
}