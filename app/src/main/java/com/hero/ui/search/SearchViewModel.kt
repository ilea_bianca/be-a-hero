package com.hero.ui.search

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hero.model.Note
import com.hero.repository.ApplicationRepository
import com.hero.repository.Result
import com.hero.ui.base.BaseViewModel
import com.hero.util.NetworkMonitor
import com.hero.util.StateLayout
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class SearchViewModel(networkMonitor: NetworkMonitor, private val repository: ApplicationRepository) :
    BaseViewModel(networkMonitor) {

    val state = ObservableField<StateLayout.State>().apply { set(StateLayout.State.LOADING) }

    private val _list = MutableLiveData<List<Note>>()
    val list: LiveData<List<Note>> = _list

    fun setSearchTerm(term: String) {
        if (term.isNotEmpty()) {
            val searchList = term.split("\\s+".toRegex()).map { it.toLowerCase(Locale.getDefault()) }
            getListNotes(searchList)
        }
    }

    private fun getListNotes(searchList: List<String>) {
        state.set(StateLayout.State.LOADING)
        viewModelScope.launch {
            repository.getNotesBySearch(searchList).also { result ->
                withContext(Dispatchers.Main) {
                    when (result) {
                        is Result.Success -> {
                            try {
                                val list = result.content as List<Note>
                                _list.value = list
                                state.set(if (list.isEmpty()) StateLayout.State.EMPTY else StateLayout.State.NORMAL)
                            } catch (e: Exception) {
                                state.set(StateLayout.State.ERROR)
                            }
                        }
                        else -> state.set(StateLayout.State.ERROR)
                    }
                }
            }
        }
    }
}