package com.hero.ui

import android.os.Bundle
import com.hero.MainBinding
import com.hero.R
import com.hero.ui.base.BaseActivity
import com.hero.ui.list.ListFragment
import com.hero.ui.map.MapFragment
import com.hero.ui.pin.PinFragment
import com.hero.ui.profile.ProfileFragment
import org.koin.android.ext.android.inject

class MainActivity : BaseActivity<MainViewModel, MainBinding>(R.layout.activity_main) {

    override val viewModel: MainViewModel by inject()
    private var lastFragmentTag = MapFragment.FRAGMENT_TAG

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.viewModel = viewModel
        addFragment(R.id.fragment_container, getLastFragment(), lastFragmentTag)
    }

    override fun onStart() {
        super.onStart()
        setupBottomNavigation()
    }

    private fun getLastFragment() = when (lastFragmentTag) {
        MapFragment.FRAGMENT_TAG -> MapFragment.newInstance()
        ListFragment.FRAGMENT_TAG -> ListFragment.newInstance()
        else -> PinFragment.newInstance()
    }

    private fun setupBottomNavigation() {
        binding.bottomNavigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.action_map -> if (lastFragmentTag != MapFragment.FRAGMENT_TAG) {
                    replaceFragment(R.id.fragment_container, MapFragment.newInstance(), MapFragment.FRAGMENT_TAG)
                    lastFragmentTag = MapFragment.FRAGMENT_TAG
                }
                R.id.action_pin -> if (lastFragmentTag != PinFragment.FRAGMENT_TAG) {
                    replaceFragment(R.id.fragment_container, PinFragment.newInstance(), PinFragment.FRAGMENT_TAG)
                    lastFragmentTag = PinFragment.FRAGMENT_TAG
                }
                R.id.action_list -> if (lastFragmentTag != ListFragment.FRAGMENT_TAG) {
                    replaceFragment(R.id.fragment_container, ListFragment.newInstance(), ListFragment.FRAGMENT_TAG)
                    lastFragmentTag = ListFragment.FRAGMENT_TAG
                }
                R.id.action_profile -> {
                    replaceFragment(
                        R.id.fragment_container,
                        ProfileFragment.newInstance(),
                        ProfileFragment.FRAGMENT_TAG
                    )
                    lastFragmentTag = ProfileFragment.FRAGMENT_TAG
                }
            }
            true
        }
    }

    override fun onResume() {
        super.onResume()
        if (lastFragmentTag != MapFragment.FRAGMENT_TAG) {
            replaceFragment(R.id.fragment_container, MapFragment.newInstance(), MapFragment.FRAGMENT_TAG)
            lastFragmentTag = MapFragment.FRAGMENT_TAG
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}