package com.hero.ui.list

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hero.model.Note
import com.hero.repository.ApplicationRepository
import com.hero.repository.Result
import com.hero.ui.base.BaseViewModel
import com.hero.util.NetworkMonitor
import com.hero.util.StateLayout
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@Suppress("UNCHECKED_CAST")
class ListViewModel(networkMonitor: NetworkMonitor, private val repository: ApplicationRepository) :
    BaseViewModel(networkMonitor) {

    private val _list = MutableLiveData<List<Note>>()
    val list: LiveData<List<Note>> = _list

    val state = ObservableField<StateLayout.State>().apply { set(StateLayout.State.LOADING) }

    init {
        getListNotes()
    }

    fun getListNotes() {
        state.set(StateLayout.State.LOADING)
        viewModelScope.launch {
            repository.getNotesByUserLocalDb().also { result ->
                withContext(Dispatchers.Main) {
                    when (result) {
                        is Result.Success<*> -> {
                            val list = result.content as List<Note>
                            _list.value = list
                            state.set(if (list.isEmpty()) StateLayout.State.EMPTY else StateLayout.State.NORMAL)
                        }
                        else -> state.set(StateLayout.State.ERROR)
                    }
                }
            }
        }
    }

    fun deleteNote(noteId: String) {
        state.set(StateLayout.State.LOADING)
        viewModelScope.launch {
            repository.deleteNote(noteId).also { result ->
                withContext(Dispatchers.Main) {
                    when (result) {
                        is Result.Success<*> -> getListNotes()
                        else -> state.set(StateLayout.State.ERROR)
                    }
                }
            }
        }
    }
}