package com.hero.ui.list

import androidx.databinding.BaseObservable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.hero.model.Note
import com.hero.model.NoteCategoryEnum

class NoteViewModel : BaseObservable() {

    val note = ObservableField<Note>()
    val isFoodCategory = MutableLiveData<Boolean>()
    val isMedicalCategory = MutableLiveData<Boolean>()
    val isOtherCategory = MutableLiveData<Boolean>()

    fun setItem(item: Note) {
        note.set(item)
        isFoodCategory.value = item.category.contains(NoteCategoryEnum.FOOD.value)
        isMedicalCategory.value = item.category.contains(NoteCategoryEnum.MEDICAL_EQUIPMENT.value)
        isOtherCategory.value = item.category.contains(NoteCategoryEnum.OTHER.value)
    }
}