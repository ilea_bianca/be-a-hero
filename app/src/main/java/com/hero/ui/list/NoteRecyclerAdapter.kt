package com.hero.ui.list

import android.content.Context
import android.view.ViewGroup
import com.hero.R
import com.hero.databinding.NoteItemBinding
import com.hero.model.Note
import com.hero.ui.base.BindingViewModelAdapter

class NoteRecyclerAdapter : BindingViewModelAdapter<NoteItemBinding, NoteViewModel>() {

    private val items = mutableListOf<Note>()
    var deleteClickListener: (String) -> Unit = { }
    var editClickListener: (String) -> Unit = { }

    fun addItems(itemList: List<Note>) {
        items.clear()
        items.addAll(itemList)
        notifyDataSetChanged()
    }

    override fun getItemLayoutId(position: Int) = R.layout.item_note

    override fun bindItem(holder: BindingViewHolder<NoteItemBinding, NoteViewModel>, position: Int, payloads: List<Any>) {
        (holder.viewModel as NoteViewModel).setItem(items[position])
    }

    override fun createViewModel(context: Context, viewType: Int) = NoteViewModel()

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingViewHolder<NoteItemBinding, NoteViewModel> {
        return super.onCreateViewHolder(parent, viewType).apply {
            val noteViewModel = viewModel ?: return@apply
            binding.edit.setOnClickListener {
                noteViewModel.note.get()?.let { note ->
                    editClickListener(note.id)
                }
            }
            binding.delete.setOnClickListener {
                noteViewModel.note.get()?.let { note ->
                    deleteClickListener(note.id)
                }
            }
        }
    }
}