package com.hero.ui.list

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.hero.ListBinding
import com.hero.R
import com.hero.ui.base.BaseFragment
import com.hero.ui.editNote.EditNoteActivity
import com.hero.util.StateLayout
import kotlinx.android.synthetic.main.error_layout.view.*
import org.koin.android.ext.android.inject

class ListFragment : BaseFragment<ListViewModel, ListBinding>(R.layout.fragment_list) {

    override val viewModel by inject<ListViewModel>()
    private val itemsAdapter = NoteRecyclerAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewModel = viewModel

        binding.itemsRecycler.layoutManager = LinearLayoutManager(requireContext())
        binding.itemsRecycler.setHasFixedSize(true)
        binding.itemsRecycler.adapter = itemsAdapter.apply {
            deleteClickListener = { noteId ->
                showDeleteDialog(noteId)
            }
            editClickListener = { noteId ->
                val intent = Intent(requireContext(), EditNoteActivity::class.java).apply {
                    putExtra(EditNoteActivity.NOTE_ID_EXTRA, noteId)
                }
                startActivity(intent)
            }
        }
        binding.listStateLayout.onStateChange { state ->
            if (state == StateLayout.State.ERROR) {
                binding.listStateLayout.button.setOnClickListener { viewModel.getListNotes() }
            }
        }

        viewModel.list.observe(requireActivity(), { list ->
            itemsAdapter.addItems(list)
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.getListNotes()
    }

    private fun showDeleteDialog(noteId: String) {
        val dialog = AlertDialog.Builder(requireContext())
            .setTitle(requireContext().getString(R.string.list_note_delete_item_title))
            .setMessage(requireContext().getString(R.string.list_note_delete_item))
            .setPositiveButton(R.string.yes) { _, _ -> viewModel.deleteNote(noteId) }
            .setNegativeButton(R.string.no, null)
            .show()
        dialog.getButton(DialogInterface.BUTTON_NEGATIVE)
            .setTextColor(resources.getColor(R.color.colorPrimaryDark, requireContext().theme))
        dialog.getButton(DialogInterface.BUTTON_POSITIVE)
            .setTextColor(resources.getColor(R.color.colorPrimaryDark, requireContext().theme))
    }

    companion object {
        const val FRAGMENT_TAG = "ListFragment"

        fun newInstance() = ListFragment()
    }
}