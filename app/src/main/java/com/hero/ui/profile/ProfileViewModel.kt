package com.hero.ui.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hero.repository.ApplicationRepository
import com.hero.ui.base.BaseViewModel
import com.hero.util.NetworkMonitor
import kotlinx.coroutines.launch

class ProfileViewModel(networkMonitor: NetworkMonitor, private val repository: ApplicationRepository) :
    BaseViewModel(networkMonitor) {

    val email: LiveData<String> = MutableLiveData<String>().apply { value = repository.email }

    fun logout() {
        viewModelScope.launch {
            repository.logout()
        }
    }

    fun deleteAccount() {
        viewModelScope.launch {
            repository.deleteAccount()
        }
    }
}