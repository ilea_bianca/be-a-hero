package com.hero.ui.profile

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.hero.ProfileBinding
import com.hero.R
import com.hero.ui.base.BaseFragment
import com.hero.ui.login.LoginActivity
import com.hero.ui.privacyPolicy.PrivacyPolicyActivity
import com.facebook.login.LoginManager
import com.google.firebase.auth.FirebaseAuth
import org.koin.android.ext.android.inject

class ProfileFragment : BaseFragment<ProfileViewModel, ProfileBinding>(R.layout.fragment_profile) {
    override val viewModel: ProfileViewModel by inject()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewModel = viewModel

        binding.profileLogout.setOnClickListener {
            showLogoutDialog()
        }
        binding.profileDeleteAccount.setOnClickListener {
            showDeleteAccountDialog()
        }
        binding.profilePrivacy.setOnClickListener {
            startActivityIntent(Intent(requireContext(), PrivacyPolicyActivity::class.java), requireActivity())
        }
    }

    private fun showDeleteAccountDialog() {
        val dialog = AlertDialog.Builder(requireContext())
            .setTitle(requireContext().getString(R.string.profile_delete_account))
            .setMessage(requireContext().getString(R.string.profile_delete_account_description))
            .setPositiveButton(R.string.yes) { _, _ ->
                viewModel.deleteAccount()
                startActivityIntent(Intent(requireContext(), LoginActivity::class.java), requireActivity())
            }
            .setNegativeButton(R.string.no, null)
            .show()
        dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(resources.getColor(R.color.colorPrimaryDark, requireContext().theme))
        dialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(resources.getColor(R.color.colorPrimaryDark, requireContext().theme))
    }

    private fun showLogoutDialog() {
        val dialog = AlertDialog.Builder(requireContext())
            .setTitle(requireContext().getString(R.string.logout_title))
            .setMessage(requireContext().getString(R.string.logout_description))
            .setPositiveButton(R.string.yes) { _, _ ->
                LoginManager.getInstance().logOut()
                FirebaseAuth.getInstance().signOut()
                viewModel.logout()
                startActivityIntent(Intent(requireContext(), LoginActivity::class.java), requireActivity())
            }
            .setNegativeButton(R.string.no, null)
            .show()
        dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(resources.getColor(R.color.colorPrimaryDark, requireContext().theme))
        dialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(resources.getColor(R.color.colorPrimaryDark, requireContext().theme))
    }

    companion object {
        const val FRAGMENT_TAG = "ProfileFragment"

        fun newInstance() = ProfileFragment()
    }
}