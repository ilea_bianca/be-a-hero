package com.hero.ui.editNote

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hero.R
import com.hero.model.Note
import com.hero.model.NoteCategoryEnum
import com.hero.repository.ApplicationRepository
import com.hero.repository.Result
import com.hero.ui.base.BaseViewModel
import com.hero.util.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class EditNoteViewModel(networkMonitor: NetworkMonitor, private val repository: ApplicationRepository) :
    BaseViewModel(networkMonitor) {

    val note = MutableLiveData<Note>()
    var noteText = ValidatorInputField("", Validator.NotEmptyValidator, R.string.add_note_description_error)
    var name = ValidatorInputField("", Validator.NotEmptyValidator, R.string.add_note_name_error)
    var phone = ValidatorInputField("", Validator.Phone, R.string.add_note_phone_error)

    var offerValue = MutableLiveData<Boolean>()
    val isFoodCategory = MutableLiveData<Boolean>()
    val isMedicalCategory = MutableLiveData<Boolean>()
    val isOtherCategory = MutableLiveData<Boolean>()
    var categoryError = mediatorLiveData(isFoodCategory, isMedicalCategory, isOtherCategory) {
        isFoodCategory.value == false && isMedicalCategory.value == false && isOtherCategory.value == false
    }

    val state = ObservableField<StateLayout.State>().apply { set(StateLayout.State.LOADING) }

    val noteEdited: LiveData<Boolean> get() = _noteEdited
    private val _noteEdited = SingleLiveEvent<Boolean>()

    fun setNoteId(noteId: String) {
        viewModelScope.launch {
            repository.getNoteByIdLocalDb(noteId).also { result ->
                state.set(StateLayout.State.NORMAL)
                if (result is Result.Success) {
                    val item = result.content
                    note.value = item
                    noteText.field.set(item.text)
                    name.field.set(item.name)
                    phone.field.set(item.phoneNumber)
                    offerValue.value = item.offer
                    isFoodCategory.value = item.category.contains(NoteCategoryEnum.FOOD.value)
                    isMedicalCategory.value = item.category.contains(NoteCategoryEnum.MEDICAL_EQUIPMENT.value)
                    isOtherCategory.value = item.category.contains(NoteCategoryEnum.OTHER.value)
                }
            }
        }
    }

    fun editNote() {
        if (isNetworkAvailable.value != true) {
            _noteEdited.value = false
            return
        }
        noteText.validate()
        phone.validate()
        name.validate()

        if (categoryError.value == false && noteText.isValid && phone.isValid && name.isValid) {
            state.set(StateLayout.State.LOADING)
            viewModelScope.launch {
                repository.editNote(
                    id = note.value?.id ?: EMPTY_STRING,
                    email = note.value?.userEmail ?: EMPTY_STRING,
                    latitude = note.value?.latitude ?: 0.0,
                    longitude = note.value?.longitude ?: 0.0,
                    address = note.value?.address ?: EMPTY_STRING,
                    offer = offerValue.value ?: true,
                    category = getCategories(),
                    name = name.value ?: EMPTY_STRING,
                    phoneNumber = phone.value ?: EMPTY_STRING,
                    text = noteText.value ?: EMPTY_STRING
                ).also { result ->
                    withContext(Dispatchers.Main) {
                        when (result) {
                            is Result.Success<*> -> _noteEdited.value = true
                            else -> _noteEdited.value = false
                        }
                        state.set(StateLayout.State.NORMAL)
                    }
                }
            }
        }
    }

    private fun getCategories(): List<Long> {
        val list = mutableListOf<Long>()
        if (isFoodCategory.value == true) list.add(NoteCategoryEnum.FOOD.value)
        if (isMedicalCategory.value == true) list.add(NoteCategoryEnum.MEDICAL_EQUIPMENT.value)
        if (isOtherCategory.value == true) list.add(NoteCategoryEnum.OTHER.value)
        return list
    }

    companion object {
        private const val EMPTY_STRING = ""
    }
}