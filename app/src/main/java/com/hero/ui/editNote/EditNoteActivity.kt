package com.hero.ui.editNote

import android.os.Bundle
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import com.hero.EditNoteBinding
import com.hero.R
import com.hero.ui.base.BaseActivity
import org.koin.android.ext.android.inject

class EditNoteActivity : BaseActivity<EditNoteViewModel, EditNoteBinding>(R.layout.activity_edit_note) {
    override val viewModel: EditNoteViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.viewModel = viewModel

        val noteId = intent.extras?.getString(NOTE_ID_EXTRA)
        noteId?.let { viewModel.setNoteId(it) }

        binding.editNoteButton.setOnClickListener {
            viewModel.editNote()
        }
        viewModel.noteEdited.observe(this, { added ->
            if (added) {
                finish()
            } else {
                Snackbar.make(binding.root, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG).show()
            }
        })
    }

    companion object {
        const val NOTE_ID_EXTRA = "noteIdExtra"
    }
}