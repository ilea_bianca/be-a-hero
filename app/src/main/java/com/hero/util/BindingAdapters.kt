package com.hero.util

import android.view.View
import androidx.annotation.StringRes
import androidx.databinding.BindingAdapter
import com.google.android.material.textfield.TextInputLayout

@BindingAdapter("error")
fun setError(view: TextInputLayout, @StringRes errorResourceId: Int) {
    if (errorResourceId == 0) {
        view.error = null
        view.isErrorEnabled = false
    } else {
        view.error = view.context.getString(errorResourceId)
    }
}

@BindingAdapter("visibleOrGone")
fun setVisibility(view: View, isVisible: Boolean) {
    view.visibility = if (isVisible) View.VISIBLE else View.GONE
}

@BindingAdapter("visibleOrInvisible")
fun setVisibleOrInvisible(view: View, isVisible: Boolean) {
    view.visibility = if (isVisible) View.VISIBLE else View.INVISIBLE
}