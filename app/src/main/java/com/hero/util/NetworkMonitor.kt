package com.hero.util

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class NetworkMonitor(application: Application) {

    private val _isNetworkConnected = MutableLiveData(true)
    val isNetworkConnected: LiveData<Boolean> = _isNetworkConnected

    private var connectivityManager: ConnectivityManager? = null
    private val networkRequest: NetworkRequest = NetworkRequest.Builder().build()
    private val networkCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            _isNetworkConnected.postValue(true)
        }

        override fun onLost(network: Network) {
            _isNetworkConnected.postValue(false)
        }
    }

    init {
        connectivityManager = application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }

    fun registerNetworkCallback() {
        connectivityManager?.registerNetworkCallback(networkRequest, networkCallback)
    }

    fun unregisterNetworkCallback() {
        connectivityManager?.unregisterNetworkCallback(networkCallback)
    }
}