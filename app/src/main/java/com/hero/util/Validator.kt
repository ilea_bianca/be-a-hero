package com.hero.util

import android.util.Patterns
import androidx.databinding.Observable
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt

sealed class Validator<T> {

    abstract fun validate(input: T?): Boolean

    object Email : Validator<String>() {
        override fun validate(input: String?) = Patterns.EMAIL_ADDRESS.matcher(input).matches()
    }

    object Password : Validator<String>() {
        override fun validate(input: String?) = input?.length ?: 0 >= 8
    }

    object Phone : Validator<String>() {
        override fun validate(input: String?): Boolean = input?.length ?: 0 > 6
    }

    object Name : Validator<String>() {
        override fun validate(input: String?): Boolean = input?.length ?: 0 >= 3
    }

    object NotEmptyValidator : Validator<String>() {
        override fun validate(input: String?) = input?.length != 0
    }
}

class ValidatorInputField<T>(initialValue: T, private val validator: Validator<T>, private val errorMessage: Int = 0) {

    val field = ObservableField(initialValue)
    val error = ObservableInt()
    val isValid get() = error.get() == 0
    val value get() = field.get()

    init {
        field.onPropertyChanged { error.set(0) }
    }

    fun validate() = error.set(if (validator.validate(field.get())) 0 else errorMessage)

    fun showError() = error.set(errorMessage)

    fun clearError() = error.set(0)

    private inline fun <T> ObservableField<T>.onPropertyChanged(crossinline callback: (T?) -> Unit) {
        addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) = callback(get())
        })
    }
}