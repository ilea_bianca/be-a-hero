package com.hero.repository

import com.hero.model.User
import com.hero.util.wrapIntoResult
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext

class LoginRepository(private val firebaseDatabase: FirebaseFirestore, private val sharedPreferencesManager: SharedPreferencesManager) {

    suspend fun saveUser(email: String, isAdmin: Boolean = false) = withContext(Dispatchers.Default) {
        wrapIntoResult {
            firebaseDatabase.collection(usersCollection)
                .document(email)
                .set(User(email, isAdmin))
                .await()
        }.also { result ->
            if (result is Result.Success) {
                saveUserEmailSharedPrefs(email)
            }
        }
    }

    private fun saveUserEmailSharedPrefs(email: String) {
        sharedPreferencesManager.userEmailField = email
    }

    fun isFirstLogin() = sharedPreferencesManager.firstLogin

    companion object {
        private const val usersCollection = "users"
    }
}