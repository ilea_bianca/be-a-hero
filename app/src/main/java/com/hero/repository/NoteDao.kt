package com.hero.repository

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.hero.model.Note

@Dao
interface NoteDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(note: Note)

    @Insert
    suspend fun insertNotes(notes: List<Note>) {
        for (note in notes) insert(note)
    }

    @Query("SELECT * FROM note WHERE id = :noteId")
    suspend fun getNoteById(noteId: String): Note

    @Query("SELECT * FROM note WHERE userEmail = :userEmail")
    suspend fun getAllNotes(userEmail: String): List<Note>

    @Query("DELETE FROM note WHERE id = :noteId")
    suspend fun delete(noteId: String): Int

    @Query("DELETE FROM note")
    suspend fun deleteAll()
}
