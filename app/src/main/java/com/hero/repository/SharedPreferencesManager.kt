package com.hero.repository

import android.content.Context
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class SharedPreferencesManager(context: Context) {
    private val preferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)
    var userEmailField by PreferenceFieldDelegate.String("user_field")
    var firstLogin by PreferenceFieldDelegate.Boolean("first_login")

    sealed class PreferenceFieldDelegate<T>(protected val key: kotlin.String) : ReadWriteProperty<SharedPreferencesManager, T> {

        class Boolean(key: kotlin.String) : PreferenceFieldDelegate<kotlin.Boolean>(key) {

            override fun getValue(thisRef: SharedPreferencesManager, property: KProperty<*>) = thisRef.preferences.getBoolean(key, true)

            override fun setValue(thisRef: SharedPreferencesManager, property: KProperty<*>, value: kotlin.Boolean) =
                thisRef.preferences.edit().putBoolean(key, value).apply()
        }

        class Int(key: kotlin.String) : PreferenceFieldDelegate<kotlin.Int>(key) {

            override fun getValue(thisRef: SharedPreferencesManager, property: KProperty<*>) = thisRef.preferences.getInt(key, 0)

            override fun setValue(thisRef: SharedPreferencesManager, property: KProperty<*>, value: kotlin.Int) =
                thisRef.preferences.edit().putInt(key, value).apply()
        }

        class String(key: kotlin.String) : PreferenceFieldDelegate<kotlin.String>(key) {

            override fun getValue(thisRef: SharedPreferencesManager, property: KProperty<*>) = thisRef.preferences.getString(key, "") ?: ""

            override fun setValue(thisRef: SharedPreferencesManager, property: KProperty<*>, value: kotlin.String) =
                thisRef.preferences.edit().putString(key, value).apply()
        }
    }

    companion object {
        private const val SHARED_PREFERENCES_NAME = "beaheroSharedPref"
    }
}