package com.hero.repository

sealed class Result<out T> {

    class Success<T>(val content: T) : Result<T>()
    class Error(val error: Exception) : Result<Nothing>()
}
