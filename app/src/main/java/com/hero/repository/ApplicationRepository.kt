package com.hero.repository

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.hero.model.Note
import com.hero.model.NoteFirestoreModel
import com.hero.util.wrapIntoResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext

class ApplicationRepository(
    database: ApplicationDatabase,
    private val firebaseDatabase: FirebaseFirestore,
    private val sharedPreferencesManager: SharedPreferencesManager,
    private val firebaseAuth: FirebaseAuth
) {

    val email = sharedPreferencesManager.userEmailField
    private val noteDao = database.noteDao()

    suspend fun saveNote(
        latitude: Double,
        longitude: Double,
        address: String,
        offer: Boolean,
        category: List<Long>,
        name: String,
        phoneNumber: String,
        text: String
    ) = withContext(Dispatchers.Default) {
        val note = Note("", email, latitude, longitude, address, offer, category, name, phoneNumber, text)
        wrapIntoResult { firebaseDatabase.collection(NOTES_COLLECTION).add(NoteFirestoreModel.fromNoteModel(note)).await() }.also { result ->
            if (result is Result.Success) {
                val document = result.content
                firebaseDatabase.collection(NOTES_COLLECTION).document(document.id).update(NOTE_ID, document.id).await()
                saveNoteLocalDb(note.apply { id = document.id })
                Result.Success(note)
            }
        }
    }

    suspend fun editNote(
        id: String,
        email: String,
        latitude: Double,
        longitude: Double,
        address: String,
        offer: Boolean,
        category: List<Long>,
        name: String,
        phoneNumber: String,
        text: String
    ) = withContext(Dispatchers.Default) {
        val note = Note(id, email, latitude, longitude, address, offer, category, name, phoneNumber, text)
        wrapIntoResult { firebaseDatabase.collection(NOTES_COLLECTION).document(id).set(NoteFirestoreModel.fromNoteModel(note)).await() }.also { result ->
            if (result is Result.Success) {
                saveNoteLocalDb(note)
            }
        }
    }

    suspend fun deleteNote(id: String) = withContext(Dispatchers.Default) {
        wrapIntoResult { firebaseDatabase.collection(NOTES_COLLECTION).document(id).delete().await() }.also { result ->
            if (result is Result.Success) {
                deleteNoteLocalDb(id)
            }
        }
    }

    suspend fun getNotesByUser() = withContext(Dispatchers.Default) {
        wrapIntoResult { firebaseDatabase.collection(NOTES_COLLECTION).whereEqualTo(NOTE_EMAIL, email).get().await() }.also { result ->
            if (result is Result.Success) {
                val list = createNotesListOfDocuments(result.content.documents)
                saveNotesLocalDb(list)
                Result.Success(list)
            }
        }
    }

    suspend fun getNotesByLatLng(minLat: Double, maxLat: Double, minLng: Double, maxLng: Double) = withContext(Dispatchers.Default) {
        val result = wrapIntoResult {
            firebaseDatabase.collection(NOTES_COLLECTION)
                .whereGreaterThanOrEqualTo(NOTE_LATITUDE, minLat)
                .whereLessThanOrEqualTo(NOTE_LATITUDE, maxLat)
                .get()
                .await()
        }
        if (result is Result.Success) {
            val list = createNotesListOfDocuments(result.content.documents).filter { it.longitude in minLng..maxLng }
            Result.Success(list)
        } else {
            result
        }
    }

    suspend fun getNotesBySearch(searchList: List<String>) = withContext(Dispatchers.Default) {
        val result = wrapIntoResult {
            firebaseDatabase.collection(NOTES_COLLECTION)
                .whereArrayContainsAny(NOTE_TEXT_ARRAY, searchList)
                .get()
                .await()
        }
        if (result is Result.Success) {
            val list = createNotesListOfDocuments(result.content.documents)
            Result.Success(list)
        } else {
            result
        }
    }

    suspend fun getNotesById(id: String) = withContext(Dispatchers.Default) {
        val result = wrapIntoResult {
            firebaseDatabase.collection(NOTES_COLLECTION)
                .whereEqualTo(NOTE_ID, id)
                .get()
                .await()
        }
        if (result is Result.Success) {
            val list = createNotesListOfDocuments(result.content.documents)
            if (list.isNullOrEmpty()) Result.Success(null)
            else Result.Success(list[0])
        } else {
            result
        }
    }

    private suspend fun saveNotesLocalDb(list: List<Note>) = withContext(Dispatchers.IO) {
        noteDao.insertNotes(list)
    }

    private suspend fun saveNoteLocalDb(note: Note) = withContext(Dispatchers.IO) {
        noteDao.insert(note)
    }

    suspend fun getNotesByUserLocalDb() = withContext(Dispatchers.IO) {
        wrapIntoResult { noteDao.getAllNotes(email) }
    }

    suspend fun getNoteByIdLocalDb(noteId: String) = withContext(Dispatchers.IO) {
        wrapIntoResult { noteDao.getNoteById(noteId) }
    }

    private suspend fun deleteNoteLocalDb(noteId: String) = withContext(Dispatchers.IO) {
        noteDao.delete(noteId)
    }

    suspend fun logout() = withContext(Dispatchers.IO) {
        noteDao.deleteAll()
        sharedPreferencesManager.userEmailField = ""
        sharedPreferencesManager.firstLogin = true
    }

    suspend fun deleteAccount() = withContext(Dispatchers.Default) {
        firebaseAuth.currentUser?.uid?.let { id ->
            firebaseDatabase.collection(NOTES_USERS)
                .document(id)
                .delete()
                .await()
        }
        wrapIntoResult {
            firebaseDatabase.collection(NOTES_COLLECTION)
                .whereEqualTo(NOTE_EMAIL, email)
                .get()
                .await()
        }.also {
            if (it is Result.Success) {
                for (document in it.content.documents) {
                    firebaseDatabase.collection(NOTES_COLLECTION)
                        .document(document.id)
                        .delete()
                        .await()
                }
            }
        }
        firebaseAuth.currentUser?.delete()
        noteDao.deleteAll()
        sharedPreferencesManager.firstLogin = true
        sharedPreferencesManager.userEmailField = ""
    }

    fun setFirstLogin(value: Boolean) {
        sharedPreferencesManager.firstLogin = value
    }

    @Suppress("UNCHECKED_CAST")
    private fun createNotesListOfDocuments(documents: List<DocumentSnapshot>): List<Note> {
        val list = mutableListOf<Note>()
        for (document in documents) {
            val id = document.id
            val email = (document.data?.get(NOTE_EMAIL) ?: EMPTY_STRING) as String
            val latitude = (document.data?.get(NOTE_LATITUDE) ?: 0.0) as Double
            val longitude = (document.data?.get(NOTE_LONGITUDE) ?: 0.0) as Double
            val address = (document.data?.get(NOTE_ADDRESS) ?: EMPTY_STRING) as String
            val offer = (document.data?.get(NOTE_OFFER) ?: false) as Boolean
            val category = (document.data?.get(NOTE_CATEGORY) ?: emptyList<Long>()) as List<Long>
            val name = (document.data?.get(NOTE_NAME) ?: EMPTY_STRING) as String
            val phoneNumber = (document.data?.get(NOTE_PHONE_NUMBER) ?: EMPTY_STRING) as String
            val text = (document.data?.get(NOTE_TEXT) ?: EMPTY_STRING) as String

            val note = Note(id, email, latitude, longitude, address, offer, category, name, phoneNumber, text)
            list.add(note)
        }
        return list
    }

    private companion object {
        const val NOTES_USERS = "users"
        const val NOTES_COLLECTION = "notes"
        const val NOTE_ID = "id"
        const val NOTE_EMAIL = "userEmail"
        const val NOTE_CATEGORY = "category"
        const val NOTE_ADDRESS = "address"
        const val NOTE_LATITUDE = "latitude"
        const val NOTE_LONGITUDE = "longitude"
        const val NOTE_OFFER = "offer"
        const val NOTE_NAME = "name"
        const val NOTE_PHONE_NUMBER = "phoneNumber"
        const val NOTE_TEXT = "text"
        const val NOTE_TEXT_ARRAY = "textArray"
        const val EMPTY_STRING = ""
    }
}