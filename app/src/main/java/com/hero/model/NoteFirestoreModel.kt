package com.hero.model

import java.util.*

data class NoteFirestoreModel(
    val userEmail: String,
    val latitude: Double,
    val longitude: Double,
    val address: String,
    val offer: Boolean,
    val category: List<Long>,
    val name: String,
    val phoneNumber: String,
    val text: String,
    val textArray: List<String>
) {
    companion object {
        fun fromNoteModel(note: Note) = NoteFirestoreModel(
            note.userEmail,
            note.latitude,
            note.longitude,
            note.address,
            note.offer,
            note.category,
            note.name,
            note.phoneNumber,
            note.text,
            note.text.split("\\s+".toRegex()).map { it.toLowerCase(Locale.getDefault()) }
        )
    }
}