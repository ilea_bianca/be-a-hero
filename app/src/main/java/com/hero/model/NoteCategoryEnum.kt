package com.hero.model

import androidx.annotation.IdRes
import com.hero.R

enum class NoteCategoryEnum(val value: Long, @IdRes val stringRes: Int) {
    FOOD(0, R.string.note_category_food),
    MEDICAL_EQUIPMENT(1, R.string.note_category_medical),
    OTHER(2, R.string.note_category_others);

    companion object {
        fun getEnum(value: Long): NoteCategoryEnum {
            values().forEach { currentType ->
                if (currentType.value == value) return currentType
            }
            return OTHER
        }
    }
}