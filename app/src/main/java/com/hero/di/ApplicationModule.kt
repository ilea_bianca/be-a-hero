package com.hero.di

import androidx.room.Room
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import com.hero.repository.ApplicationDatabase
import com.hero.repository.ApplicationRepository
import com.hero.repository.LoginRepository
import com.hero.repository.SharedPreferencesManager
import com.hero.ui.MainViewModel
import com.hero.ui.addNote.AddNoteViewModel
import com.hero.ui.detail.DetailViewModel
import com.hero.ui.editNote.EditNoteViewModel
import com.hero.ui.info.InfoViewModel
import com.hero.ui.launch.LaunchViewModel
import com.hero.ui.list.ListViewModel
import com.hero.ui.login.LoginViewModel
import com.hero.ui.map.MapViewModel
import com.hero.ui.pin.PinViewModel
import com.hero.ui.profile.ProfileViewModel
import com.hero.ui.search.SearchViewModel
import com.hero.util.NetworkMonitor
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val serviceModule = module {
    single { Firebase.firestore }
    single { FirebaseAuth.getInstance() }
    single { NetworkMonitor(androidApplication()) }
}

val repositoryModule = module {
    single { Room.databaseBuilder(androidApplication(), ApplicationDatabase::class.java, ApplicationDatabase.name).build() }
    single { SharedPreferencesManager(androidApplication().applicationContext) }

    factory { Gson() }
    factory { LoginRepository(get(), get()) }
    factory { ApplicationRepository(get(), get(), get(), get()) }
}

val viewModelModule = module {
    viewModel { LaunchViewModel(get(), get()) }
    viewModel { LoginViewModel(get(), get()) }
    viewModel { MainViewModel(get()) }
    viewModel { MapViewModel(get(), get()) }
    viewModel { PinViewModel(get()) }
    viewModel { ListViewModel(get(), get()) }
    viewModel { ProfileViewModel(get(), get()) }
    viewModel { AddNoteViewModel(get(), get()) }
    viewModel { EditNoteViewModel(get(), get()) }
    viewModel { DetailViewModel(get(), get()) }
    viewModel { SearchViewModel(get(), get()) }
    viewModel { InfoViewModel(get(), get()) }
}