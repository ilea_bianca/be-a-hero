package com.hero

import android.app.Application
import com.facebook.stetho.Stetho
import com.hero.di.repositoryModule
import com.hero.di.serviceModule
import com.hero.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class HeroApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@HeroApplication)
            modules(listOf(serviceModule, repositoryModule, viewModelModule))
        }
        Stetho.initializeWithDefaults(this)
    }
}
